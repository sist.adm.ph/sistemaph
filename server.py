#!/usr/bin/env python

from flask import Flask, render_template, request, redirect, url_for, g, flash, Response
from flask_bootstrap import Bootstrap
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, DateField, SubmitField, validators, Form, SelectField
from wtforms.validators import InputRequired, Email, Length
from flask_sqlalchemy  import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
import os
import requests
import sqlite3
import gevent
import pdfkit
from gevent.pywsgi import WSGIServer
from gevent.queue import Queue
import time, datetime, json
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from flask_datepicker import datepicker
import sys


# class ServerSentEvent(object):

    # def __init__(self, data):
    #     self.data = data
    #     self.event = None
    #     self.id = None
    #     self.desc_map = {
    #         self.data : "data",
    #         self.event : "event",
    #         self.id : "id"
    #     }

    # def encode(self):
    #     if not self.data:
    #         return ""
    #     lines = ["%s: %s" % (v, k) 
    #              for k, v in self.desc_map.iteritems() if k]
        
    #     return "%s\n\n" % "\n".join(lines)

app = Flask(__name__)


sdnController="10.0.1.8"

app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
file_path = os.path.abspath(os.getcwd())+"/database.db"
app.config['SECRET_KEY'] = 'Thisissupposedtobesecret!'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///'+file_path
bootstrap = Bootstrap(app)
datepicker(app)
db = SQLAlchemy(app)
login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

def dateDiffInSeconds(date1, date2):
  timedelta = date2 - date1
  return timedelta.days * 24 * 3600 + timedelta.seconds

def daysHoursMinutesSecondsFromSeconds(seconds):
	minutes, seconds = divmod(seconds, 60)
	hours, minutes = divmod(minutes, 60)
	days, hours = divmod(hours, 24)
	return (days, hours, minutes, seconds)

class DateForm(FlaskForm):
    dt = DateField('Pick a Date', format="%m/%d/%Y")

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(15), unique=True)
    email = db.Column(db.String(50), unique=True)
    phone = db.Column(db.String(20))
    password = db.Column(db.String(80))
    limited_date = db.Column(db.String(120))
    creation_date = db.Column(db.String(120))
    admin_privilege = db.Column(db.Integer)
    is_terapist = db.Column(db.String(3))

class SecurityPolicy(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(120))
    publisher = db.Column(db.String(120))
    description = db.Column(db.String(120))
    category = db.Column(db.String(40))
    url = db.Column(db.String(100))
    port = db.Column(db.String(6))
    created_on = db.Column(db.String(20))
    created_by = db.Column(db.String(40))

class ControllerConfig(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80))
    server_ip = db.Column(db.String(80))
    output_port = db.Column(db.String(10))
    default_port = db.Column(db.String(10))

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

class LoginForm(FlaskForm):
    username = StringField('Nombre de usuario', validators=[InputRequired(), Length(min=4, max=15)])
    password = PasswordField('Contraseña', validators=[InputRequired(), Length(min=8, max=80)])
    remember = BooleanField('Recuérdame ')

class RegisterForm(FlaskForm):
    email = StringField('Correo Electrónico ', validators=[InputRequired(), Email(message='Email Invalido'), Length(max=50)])
    phone = StringField('Teléfono ', validators=[InputRequired(), Length(min=10, max=10)])
    username = StringField('Nombre de usuario', validators=[InputRequired(), Length(min=4, max=15)])   
    password = PasswordField('Contraseña', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message='Contraseña debe Coincidir')
    ])
    confirm = PasswordField('Repetir Contraseña')
    accept_tos = BooleanField('Acepto los Términos ', [validators.DataRequired()])
##########/

class SecurityPolicyForm(FlaskForm):
    name = StringField('Nombre', validators=[InputRequired("Ingrese un Nombre"), Length(max=100)])
    publisher = StringField('publisher', validators=[InputRequired("Ingrese un Fabricante"), Length(max=100)])
    description = StringField('description', validators=[InputRequired("Ingrese un Descripcion"), Length(max=120)])
    category = StringField('category', validators=[InputRequired("Ingrese un Categoria"), Length(max=40)])
    url = StringField('url', validators=[InputRequired("Ingrese un URL"), Length(max=100)])
    port = StringField('port', validators=[InputRequired("Ingrese un Puerto"), Length(max=6)])
    submit = SubmitField('Guardar')

# class PatientSearchForm(Form):    
#     search = StringField('')

DATABASE = "database.db"

# Gestión de la base de datos.

def get_db():
    db = getattr(g, '_database', None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE)
        db.row_factory = sqlite3.Row
    return db

def query_db(query, args=(), one=False):
    cur = get_db().execute(query, args)
    rv = cur.fetchall()
    cur.close()
    return (rv[0] if rv else None) if one else rv

def change_db(query,args=()):
    cur = get_db().execute(query, args)
    get_db().commit()
    cur.close()

@app.teardown_appcontext
def close_connection(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

# URL de enrutamiento y procesamientos.
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/adminpanel')
@login_required
def adminpanel():
    if current_user.admin_privilege == 1 :
        user_list=query_db("SELECT * FROM user")
        #limited_date_object = datetime.datetime.strptime(current_user.limited_date, '%Y-%m-%d %H:%M:%S')
        return render_template("adminpanel.html",current_user=current_user,user_list=user_list, actualdate = datetime.datetime.now(), datetime = datetime)

    elif current_user.admin_privilege == 2 or  current_user.admin_privilege == 3 :
        user_list=query_db("SELECT * FROM access ORDER BY id DESC ")
        #limited_date_object = datetime.datetime.strptime(current_user.limited_date, '%Y-%m-%d %H:%M:%S')
        return render_template("adminpanel.html",current_user=current_user,user_list=user_list, actualdate = datetime.datetime.now(), datetime = datetime)
    
    else:
        return('<h1>Su actual usuario no es administrador</h1>')


@app.route('/policyautocomplete', methods=['GET'])
def policyautocomplete():
    search = request.args.get('qry')
    policies_list =  query_db("SELECT name FROM security_policy")
    return Response(json.dumps(policies_list), mimetype='application/json')

@app.route('/policieslist', methods=['GET', 'POST'])
@login_required
def policieslist():
    policies_list =  query_db("SELECT * FROM security_policy")
    return render_template("policieslist.html"
        , current_user = current_user
        , policies_list = policies_list)



@app.route('/policiesform', methods=['GET', 'POST'])
@login_required
def policiesform():

    form = SecurityPolicyForm()

    if request.method == "GET":
        return render_template("policiesform.html", form=form)
    elif request.method == "POST":    
        new_securityPolicy = SecurityPolicy(name = form.name.data.title()
            , publisher = form.publisher.data.title()
            , description = form.description.data
            , category = form.category.data.title()
            , url = form.url.data.lower()
            , port = form.port.data
            , created_on = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            , created_by = current_user.username)
        db.session.add(new_securityPolicy)
        db.session.commit()
        return redirect(url_for("policieslist"))
    else:
        return('<h1>Su actual usuario no es administrador.</h1>')

@app.route('/policyupdate/<int:id>', methods=['GET', 'POST'])
@login_required
def policyupdate(id):
    
    policy = SecurityPolicyForm()

    if request.method == "GET" and current_user.admin_privilege == 1:
        policy = query_db("SELECT * FROM security_policy WHERE id=?", [id], one=True)
        return render_template("policyupdate.html", policy=policy)
    elif request.method == "POST" and current_user.admin_privilege == 1:
        values = [policy.name.data.title() , policy.publisher.data.title(), policy.description.data, policy.category.data.title(), policy.url.data.lower(), policy.port.data, id]
        change_db("UPDATE security_policy SET name=?, publisher=?, description=?, category=?, url=?, port=? WHERE id=?", values)
        return redirect(url_for("policieslist"))
    else:
        return('<h1>Su actual usuario no es administrador.</h1>')

@app.route('/policydelete/<int:id>', methods=['GET', 'POST'])
@login_required
def policydelete(id):
    policy = SecurityPolicyForm()

    if request.method == "GET":
        policy = query_db("SELECT * FROM security_policy WHERE id=?", [id], one=True)
        return render_template("policydelete.html", policy=policy)
    elif request.method == "POST":
        change_db("DELETE FROM security_policy WHERE id = ?",[id])
        return redirect(url_for("policieslist"))
    else:
        return('<h1>Su actual usuario no es administrador.</h1>')

@app.route('/newaccessrequest')
@login_required
def newaccessrequest():
    return render_template(
        'newaccessrequest.html',
        data = query_db("SELECT * FROM security_policy"),
        access = None
    )

@app.route('/commitaccessrequest', methods=["GET", "POST"])
@login_required
def commitaccessrequest():
    if request.method == "POST":
        select = request.form.to_dict()
        fromDate = datetime.datetime.strptime( select["from_date"], '%Y-%m-%d %H:%M').strftime('%Y-%m-%d %H:%M:%S')
        endDate = datetime.datetime.strptime( select["end_date"], '%Y-%m-%d %H:%M').strftime('%Y-%m-%d %H:%M:%S')
        securityPolicy = select['policy_select']
        securityPolicyObj = query_db("SELECT * FROM security_policy WHERE name LIKE ?", [securityPolicy], one=True)
        values = [current_user.username
            , securityPolicyObj['url']
            , endDate
            , select['reason']
            , fromDate
            , endDate
        ]
        change_db("INSERT INTO access (userid, urlaccess, limited_date, reason, from_date, end_date) VALUES (?,?,?,?,?,?)",values)
        
        if current_user.admin_privilege == 1:
            return redirect(url_for("accesslist"))
        else:
            #return redirect(url_for("requestdone"))
            return (fromDate)
############################################################################
# @app.route('/createuser')
# @login_manager
# def createuser():
#     if try:
#         current_user.admin_privilege == 1:
#         accest_list= query ("SELECT * from users")
#         return render_template("createuser.html", current_user=current_user, access_list=acces_list, actual_date= datetime.datetime.now)
#         pass
#     except expression as identifier:
#         write("Scheduler")        
#         pass


@app.route('/accesslist')
@login_required
def accesslist():
    if current_user.admin_privilege == 1 or current_user.admin_privilege == 2 or current_user.admin_privilege == 3:
        access_list=query_db("SELECT * FROM access ORDER BY id DESC")
        # rendered = render_template('accesslist.html',current_user=current_user,access_list=access_list, actualdate = datetime.datetime.now(), datetime = datetime, len=len)
        # pdf = pdfkit.from_string(rendered, False)

        # response = make_response(pdf)
        # response.headers['Content-Type'] = 'application/pdf'
        # response.headers['Content-Disposition'] = 'inline; filename=output.pdf'
        # #pdfkit.from_file('accesslist.html', 'out.pdf')
        #limited_date_object = datetime.datetime.strptime(current_user.limited_date, '%Y-%m-%d %H:%M:%S')
        return render_template("accesslist.html",current_user=current_user,access_list=access_list, actualdate = datetime.datetime.now(), datetime = datetime, len=len)
        #return response
    else:
        return('<h1>Su actual usuario no es administrador.</h1>')

@app.route('/todayaccesslist', methods = ['GET'])
def todayaccesslist():
    today = datetime.datetime.now()
    
    tomorrow = datetime.datetime(today.year, today.month, today.day + 1)
    current_date = today.strftime('%Y-%m-%d %H:%M:%S')
    tomorrow_date = tomorrow.strftime('%Y-%m-%d %H:%M:%S')
    access_list = query_db("SELECT * FROM access WHERE approve == 1 AND from_date >= ? AND from_date <= ?", [current_date], [tomorrow_date])
    if len(access_list) > 0:
        responseData = []
        for objAccess in access_list:
            values = {'username' : objAccess['userid']
                , 'url_port' : objAccess['urlaccess']
                , 'limited_date' : objAccess['limited_date']
                , 'reason' : objAccess['reason']
                , 'from_date' : objAccess['from_date']
                , 'end_date' : objAccess['end_date']
            }
            responseData.append(values)
        jsReponse = json.dumps(responseData)
        resp = Response(jsReponse, status = 200, mimetype = 'application/json')
        return (resp)
    else:
        jsReponse = json.dumps('None')
        resp = Response(jsReponse, status = 404, mimetype = 'application/json')
        return (resp)    

@app.route('/allaccesslist', methods = ['GET'])
def allaccesslist():    
    access_list = query_db("SELECT * FROM access WHERE approve == 1")
    if len(access_list) > 0:
        responseData = []
        for objAccess in access_list:
            values = {'username' : objAccess['userid']
                , 'url_port' : objAccess['urlaccess']
                , 'limited_date' : objAccess['limited_date']
                , 'reason' : objAccess['reason']
                , 'from_date' : objAccess['limited_date']
                , 'end_date' : objAccess['initial_date']
            }
            responseData.append(values)
        
        jsReponse = json.dumps(responseData)
        resp = Response(jsReponse, status = 200, mimetype = 'application/json')
        return (resp)
    else:
        jsReponse = json.dumps('None')
        resp = Response(jsReponse, status = 404, mimetype = 'application/json')
        return (resp)  

##############################################################################################################
# @app.route('/search', methods=['GET', 'POST'])
# def search():
#     query = request.args('search')

# @app.route('/search', methods=['GET', 'POST'])
# @login_required
# def search():
#     form = SearchForm()
#     if request.method == 'POST' and form.validate_on_submit():
#         return redirect((url_for('search_results', query=form.search.data)))  # or what you want
#     return render_template('search.html', form=form)


@app.route('/showform/<int:id>', methods=['GET', 'POST'])
@login_required
def showform(id):    
        if request.method == "GET":             
            return render_template("showform1.html", access=None)

        if request.method == "POST":
            access=request.form.to_dict()
            values=[current_user.username,access['form']]            
            change_db("INSERT INTO show_form (userid,form) VALUES (?,?)",values)
        
            if values[1] == "DATOS GENERALES":                
                datos=query_db("SELECT * FROM datos_generales WHERE id=?",[id],one=True)
                return render_template("show_datos_generales1.html",datos_generales=datos)

            if values[1] == "VALORACION NIVEL 1 TERAPEUTA":                
                datos=query_db("SELECT * FROM val_n1_terap WHERE id=?",[id],one=True)
                return render_template("show_val_n1_terap1.html",val_n1_terap=datos)

            if values[1] == "VALORACION GRUPO HISTORICO TERAPEUTA":                
                datos=query_db("SELECT * FROM valo_grup_hist WHERE id=?",[id],one=True)
                return render_template("show_grupo_historico1.html",valo_grup_hist=datos)
            
            if values[1] == "VALORACION NIVEL 2 TERAPEUTA":                
                datos=query_db("SELECT * FROM val_n2_terap WHERE id=?",[id],one=True)
                return render_template("show_val_n2_terap_1.html",val_n2_terap=datos)

            if values[1] == "SONDA AFECTIVO-SEXUAL I":                
                datos=query_db("SELECT * FROM sond_afect_sex WHERE id=?",[id],one=True)
                return render_template("show_sonda_sexual_1.html",sond_afect_sex=datos)

            if values[1] == "SONDA DE ALCOHOL":                
                datos=query_db("SELECT * FROM sond_alcohol WHERE id=?",[id],one=True)
                return render_template("show_sonda_alcoh_1.html",sond_alcohol=datos)

            if values[1] == "VALORACION NIVEL 3 TERAPEUTA":                
                datos=query_db("SELECT * FROM val_n3_terap WHERE id=?",[id],one=True)
                return render_template("show_val_n3_terap_1.html",val_n3_terap=datos)

            if values[1] == "VALORACION FASE DE REINSERCION":                
                datos=query_db("SELECT * FROM val_fas_reins WHERE id=?",[id],one=True)
                return render_template("show_fase_reinser_1.html",val_fas_reins=datos)

            if values[1] == "VALORACION FINAL":                
                datos=query_db("SELECT * FROM valora_final WHERE id=?",[id],one=True)
                return render_template("show_valor_final_1.html",valora_final=datos)
                            
            
##############################################################################################################
@app.route('/createform/<int:id>', methods=['GET', 'POST'])
@login_required
def createform(id):    
        if request.method == "GET":           
            return render_template("createform.html", access=None)
        
        if request.method == "POST":
            access=request.form.to_dict()
            values=[current_user.username,access['form']]            
            change_db("INSERT INTO patient_form (userid,form) VALUES (?,?)",values)
            #form_selected = query_db("SELECT form FROM patient_form ORDER BY id DESC LIMIT 1")    
            
            if values[1] == "DATOS GENERALES":
                return render_template("datos_generales1.html", access=None)
                if request.method == "POST":
                    entries=request.form.to_dict()
                    values=[entries["name"],entries["last_name"],entries["born_date"],entries["born_place"],entries["id_number"],entries["ocupation"],entries["academic_level"],entries["civil_state"],entries["direction"],entries["phone"],entries["contact_1"],entries["phone_1"],entries["contact_2"],entries["phone_2"]]
                    change_db("INSERT INTO datos_generales (name,last_name,born_date,born_place,id_number,ocupation,academic_level,civil_state,direction,phone,contact_1,phone_1,contact_2,phone_2) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)",values)
                    return redirect(url_for("accesslist"))

            if values[1] == "VALORACION NIVEL 1 TERAPEUTA":
                return render_template("europasi_1.html", access=None)
                if request.method == "POST":
                    entries=request.form.to_dict()
                    values=[entries["date"],entries["program"],entries["user_name"],entries["terape"],entries["ident_reco_adicc"],entries["ref_mot_cam"],entries["dist_mund_dro"],entries["adqu_perso_desen"],entries["asum_respon"],entries["conoc_acep_norm"],entries["apre_contro_reca"],entries["hacer_intr_vida"],entries["iden_senti"],entries["mejo_auto"]]
                    change_db("INSERT INTO val_n1_terap (date,program,user_name,terape,ident_reco_adicc,ref_mot_cam,dist_mund_dro,adqu_perso_desen,asum_respon,conoc_acep_norm,apre_contro_reca,hacer_intr_vida,iden_senti,mejo_auto) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)",values)
                    return redirect(url_for("accesslist"))

            if values[1] == "VALORACION GRUPO HISTORICO TERAPEUTA":
                return render_template("grupo_historico_1.html", access=None)
                if request.method == "POST":
                    entries=request.form.to_dict()
                    values=[entries["date"],entries["program"],entries["user_name"],entries["terape"],entries["tema_especi"],entries["aspec_relev"],entries["act_usua_gru"],entries["linea_trabaj_gru"],entries["tema_sin_abor"],entries["coment"]]
                    change_db("INSERT INTO valo_grup_hist (date,program,user_name,terape,tema_especi,aspec_relev,act_usua_gru,linea_trabaj_gru,tema_sin_abor,coment) VALUES (?,?,?,?,?,?,?,?,?,?)",values)
                    return redirect(url_for("accesslist"))
            
            if values[1] == "VALORACION NIVEL 2 TERAPEUTA":
                return render_template("valo_n2_terap_1.html", access=None)
                if request.method == "POST":
                    entries=request.form.to_dict()
                    values=[entries["date"],entries["program"],entries["user_name"],entries["terape"],entries["asum_resp_per"],entries["iden_reco_gest"],entries["anal_pas_pers"],entries["mejo_recupe"],entries["iden_pensa"],entries["desarr_habil"],entries["org_tiemp_libr"],entries["coment"]]
                    change_db("INSERT INTO val_n2_terap (date,program,user_name,terape,asum_resp_per,iden_reco_gest,anal_pas_pers,mejo_recupe,iden_pensa,desarr_habil,org_tiemp_libr,coment) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",values)
                    return redirect(url_for("accesslist"))

            if values[1] == "SONDA AFECTIVO-SEXUAL I":
                return render_template("sonda_sexual_1.html", access=None)
                if request.method == "POST":
                    entries=request.form.to_dict()
                    values=[entries["date"],entries["program"],entries["user_name"],entries["terape"],entries["iden_gen"],entries["orien_sexu"],entries["abus_sexu"],entries["abus_sexu_per"],entries["coment"]]
                    change_db("INSERT INTO sond_afect_sex (date,program,user_name,terape,iden_gen,orien_sexu,abus_sexu,abus_sexu_per,coment) VALUES (?,?,?,?,?,?,?,?,?)",values)
                    return redirect(url_for("accesslist"))

            if values[1] == "SONDA DE ALCOHOL":
                return render_template("sonda_alcoh_1.html", access=None)
                if request.method == "POST":
                    entries=request.form.to_dict()
                    values=[entries["date"],entries["program"],entries["user_name"],entries["terape"],entries["edad_inic"],entries["intr_consu_alco"],entries["frec_cons_viol"],entries["frec_cons_alco_drog"],entries["cons_alc_jueg"],entries["indent_prob_alco"],entries["coment"]]
                    change_db("INSERT INTO sond_alcohol (date,program,user_name,terape,edad_inic,intr_consu_alco,frec_cons_viol,frec_cons_alco_drog,cons_alc_jueg,indent_prob_alco,coment) VALUES (?,?,?,?,?,?,?,?,?,?,?)",values)
                    return redirect(url_for("accesslist"))

            if values[1] == "VALORACION NIVEL 3 TERAPEUTA":
                return render_template("valo_n3_terap_1.html", access=None)
                if request.method == "POST":
                    entries=request.form.to_dict()
                    values=[entries["date"],entries["program"],entries["user_name"],entries["terape"],entries["prof_cons_anter"],entries["incor_habi_est"],entries["mej_relaci"],entries["mane_emoc_sent"],entries["modif_rac_irra"],entries["desarr_cap_pro"],entries["ocupa_tiem_incom_drog"],entries["desa_motiv_educa"],entries["coment"]]
                    change_db("INSERT INTO val_n3_terap (date,program,user_name,terape,prof_cons_anter,incor_habi_est,mej_relaci,mane_emoc_sent,modif_rac_irra,desarr_cap_pro,ocupa_tiem_incom_drog,desa_motiv_educa,coment) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",values)
                    return redirect(url_for("accesslist"))

            if values[1] == "VALORACION FASE DE REINSERCION":
                return render_template("fase_reinser_1.html", access=None)
                if request.method == "POST":
                    entries=request.form.to_dict()
                    values=[entries["date"],entries["program"],entries["user_name"],entries["terape"],entries["logr_inser_edu_lab"],entries["esta_rel_soci_riesg"],entries["mante_rela_soc"],entries["mante_auto_amena"],entries["coment"]]
                    change_db("INSERT INTO val_fas_reins (date,program,user_name,terape,logr_inser_edu_lab,esta_rel_soci_riesg,mante_rela_soc,mante_auto_amena,coment) VALUES (?,?,?,?,?,?,?,?,?)",values)
                    return redirect(url_for("accesslist"))
            
            if values[1] == "VALORACION FINAL":
                return render_template("valor_final_1.html", access=None)
                if request.method == "POST":
                    entries=request.form.to_dict()
                    values=[entries["date"],entries["program"],entries["user_name"],entries["terape"],entries["familia"],entries["emocional"],entries["cognitivo"],entries["conduct"],entries["social"],entries["educ_labo"],entries["coment"]]
                    change_db("INSERT INTO valora_final (date,program,user_name,terape,familia,emocional,cognitivo,conduct,social,educ_labo,coment) VALUES (?,?,?,?,?,?,?,?,?,?,?)",values)
                    return redirect(url_for("requestdoner"))

            else:
                return render_template("404.html")

@app.route('/donaciones', methods=['GET', 'POST'])
def donaciones():    
        if request.method == "GET":           
            return render_template("donaciones_1.html", access=None)
        
        if request.method == "POST":
            entries=request.form.to_dict()
            values=[entries["nombre"],entries["empresa"],entries["correo"],entries["telefono"],entries["coment"]]
            change_db("INSERT INTO donaciones (nombre,empresa,correo,telefono,coment) VALUES (?,?,?,?,?)",values)

             ############ ENVIO DE CORREO ###################################################

        
            fromaddr = values[2]
            toaddr = "sist.donaciones.ph@gmail.com"
            msg = MIMEMultipart()
            msg['From'] = fromaddr
            msg['To'] = toaddr
            msg['Subject'] = "Solicitud de Donacion Recibida"
            
            body = "Se ha realizado una solcitud para donacion por parte de: " + values[0]+ ", por parte de la empresa: "+ values[1] + ", con el correo " + values[2] + ", Favor contactar al numero: " + values[3] + ", A continuacion comentario del interesado: " + values[4]  
            # button1 = """
            #         <html>
            #         <head></head>
            #         <body>
            #         <br>
            #             <td>
            #             # <a href="http://localhost:5000/login">Aprobar</a>
            #             # <a href="http://localhost:5000/login">Rechazar</a>
            #             </td>
            #         </body>
            #         </html>
            #         """
            
            msg.attach(MIMEText(body, 'plain'))
            part1 = MIMEText('html')
            # part2 = MIMEText(" \nhttp://localhost:5000/approverequest/{{access.id}}" )
            # part3 = MIMEText(" \nhttp://localhost:5000/rejectrequest/{{access.id}}")        
            msg.attach(part1)
            # msg.attach(part3)
            
            server = smtplib.SMTP('smtp.gmail.com', 587)
            server.starttls()
            server.login(toaddr, "Prueba123*")
            text = msg.as_string()
            server.sendmail(fromaddr, toaddr, text)
            server.quit()
        ###############################################################################
            return '<h1>Su donacion ha sido registrada exitosamente, le contactaremos!! </h1>'

@app.route('/datosgenerales', methods=['GET', 'POST'])
@login_required
def datosgenerales():    
        if request.method == "GET":           
            return render_template("datos_generales1.html", access=None)
        
        if request.method == "POST":
            entries=request.form.to_dict()
            values=[entries["name"],entries["last_name"],entries["born_date"],entries["born_place"],entries["id_number"],entries["ocupation"],entries["academic_level"],entries["civil_state"],entries["direction"],entries["phone"],entries["contact_1"],entries["phone_1"],entries["contact_2"],entries["phone_2"]]
            change_db("INSERT INTO datos_generales (name,last_name,born_date,born_place,id_number,ocupation,academic_level,civil_state,direction,phone,contact_1,phone_1,contact_2,phone_2) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)",values)
            return redirect(url_for("accesslist"))
        #return redirect(url_for("requestdone"))

@app.route('/europasi1', methods=['GET', 'POST'])
@login_required
def europasi1():    
        if request.method == "GET":           
            return render_template("europasi_1.html", access=None)
        
        if request.method == "POST":
            entries=request.form.to_dict()
            values=[entries["date"],entries["program"],entries["user_name"],entries["terape"],entries["ident_reco_adicc"],entries["ref_mot_cam"],entries["dist_mund_dro"],entries["adqu_perso_desen"],entries["asum_respon"],entries["conoc_acep_norm"],entries["apre_contro_reca"],entries["hacer_intr_vida"],entries["iden_senti"],entries["mejo_auto"]]
            change_db("INSERT INTO val_n1_terap (date,program,user_name,terape,ident_reco_adicc,ref_mot_cam,dist_mund_dro,adqu_perso_desen,asum_respon,conoc_acep_norm,apre_contro_reca,hacer_intr_vida,iden_senti,mejo_auto) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)",values)
            return redirect(url_for("accesslist"))

@app.route('/grupohistorico', methods=['GET', 'POST'])
@login_required
def grupohistorico():    
        if request.method == "GET":           
            return render_template("grupo_historico_1.html", access=None)
        
        if request.method == "POST":
            entries=request.form.to_dict()
            values=[entries["date"],entries["program"],entries["user_name"],entries["terape"],entries["tema_especi"],entries["aspec_relev"],entries["act_usua_gru"],entries["linea_trabaj_gru"],entries["tema_sin_abor"],entries["coment"]]
            change_db("INSERT INTO valo_grup_hist (date,program,user_name,terape,tema_especi,aspec_relev,act_usua_gru,linea_trabaj_gru,tema_sin_abor,coment) VALUES (?,?,?,?,?,?,?,?,?,?)",values)
            return redirect(url_for("accesslist"))

@app.route('/valora_n2_terap', methods=['GET', 'POST'])
@login_required
def valora_n2_terap():    
        if request.method == "GET":           
            return render_template("valora_n2_terap_1.html", access=None)
        
        if request.method == "POST":
            entries=request.form.to_dict()
            values=[entries["date"],entries["program"],entries["user_name"],entries["terape"],entries["asum_resp_per"],entries["iden_reco_gest"],entries["anal_pas_pers"],entries["mejo_recupe"],entries["iden_pensa"],entries["desarr_habil"],entries["org_tiemp_libr"],entries["coment"]]
            change_db("INSERT INTO val_n2_terap (date,program,user_name,terape,asum_resp_per,iden_reco_gest,anal_pas_pers,mejo_recupe,iden_pensa,desarr_habil,org_tiemp_libr,coment) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)",values)
            return redirect(url_for("accesslist"))

@app.route('/sonda_sexual', methods=['GET', 'POST'])
@login_required
def sonda_sexual():    
        if request.method == "GET":           
            return render_template("sonda_sexual_1.html", access=None)
        
        if request.method == "POST":
            entries=request.form.to_dict()
            values=[entries["date"],entries["program"],entries["user_name"],entries["terape"],entries["iden_gen"],entries["orien_sexu"],entries["abus_sexu"],entries["abus_sexu_per"],entries["coment"]]
            change_db("INSERT INTO sond_afect_sex (date,program,user_name,terape,iden_gen,orien_sexu,abus_sexu,abus_sexu_per,coment) VALUES (?,?,?,?,?,?,?,?,?)",values)
            return redirect(url_for("accesslist"))

@app.route('/sonda_alcohol', methods=['GET', 'POST'])
@login_required
def sonda_alcohol():    
        if request.method == "GET":           
            return render_template("sonda_alcoh_1.html", access=None)
        
        if request.method == "POST":
            entries=request.form.to_dict()
            values=[entries["date"],entries["program"],entries["user_name"],entries["terape"],entries["edad_inic"],entries["intr_consu_alco"],entries["frec_cons_viol"],entries["frec_cons_alco_drog"],entries["cons_alc_jueg"],entries["indent_prob_alco"],entries["coment"]]
            change_db("INSERT INTO sond_alcohol (date,program,user_name,terape,edad_inic,intr_consu_alco,frec_cons_viol,frec_cons_alco_drog,cons_alc_jueg,indent_prob_alco,coment) VALUES (?,?,?,?,?,?,?,?,?,?,?)",values)
            return redirect(url_for("accesslist"))

@app.route('/valora_n3_terap', methods=['GET', 'POST'])
@login_required
def valora_n3_terap():    
        if request.method == "GET":           
            return render_template("valora_n3_terap_1.html", access=None)
        
        if request.method == "POST":
            entries=request.form.to_dict()
            values=[entries["date"],entries["program"],entries["user_name"],entries["terape"],entries["prof_cons_anter"],entries["incor_habi_est"],entries["mej_relaci"],entries["mane_emoc_sent"],entries["modif_rac_irra"],entries["desarr_cap_pro"],entries["ocupa_tiem_incom_drog"],entries["desa_motiv_educa"],entries["coment"]]
            change_db("INSERT INTO val_n3_terap (date,program,user_name,terape,prof_cons_anter,incor_habi_est,mej_relaci,mane_emoc_sent,modif_rac_irra,desarr_cap_pro,ocupa_tiem_incom_drog,desa_motiv_educa,coment) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",values)
            return redirect(url_for("accesslist"))

@app.route('/fase_reinser', methods=['GET', 'POST'])
@login_required
def fase_reinser():    
        if request.method == "GET":           
            return render_template("fase_reinser_1.html", access=None)
        
        if request.method == "POST":
            entries=request.form.to_dict()
            values=[entries["date"],entries["program"],entries["user_name"],entries["terape"],entries["logr_inser_edu_lab"],entries["esta_rel_soci_riesg"],entries["mante_rela_soc"],entries["mante_auto_amena"],entries["coment"]]
            change_db("INSERT INTO val_fas_reins (date,program,user_name,terape,logr_inser_edu_lab,esta_rel_soci_riesg,mante_rela_soc,mante_auto_amena,coment) VALUES (?,?,?,?,?,?,?,?,?)",values)
            return redirect(url_for("accesslist"))

@app.route('/valor_final', methods=['GET', 'POST'])
@login_required
def valor_final():    
        if request.method == "GET":           
            return render_template("valor_final_1.html", access=None)
        
        if request.method == "POST":
            entries=request.form.to_dict()
            values=[entries["date"],entries["program"],entries["user_name"],entries["terape"],entries["familia"],entries["emocional"],entries["cognitivo"],entries["conduct"],entries["social"],entries["educ_labo"],entries["coment"]]
            change_db("INSERT INTO valora_final (date,program,user_name,terape,familia,emocional,cognitivo,conduct,social,educ_labo,coment) VALUES (?,?,?,?,?,?,?,?,?,?,?)",values)
            return redirect(url_for("accesslist"))
                
##########################################################################################################################################

@app.route('/create', methods=['GET', 'POST'])
@login_required
def create():

    if request.method == "GET":
        return render_template("create.html", data = query_db("SELECT * FROM security_policy"), access=None)

    if request.method == "POST":
        access=request.form.to_dict()
        values=[current_user.username,access["urlaccess"],access["initial_date"],access["reason"]]
        change_db("INSERT INTO access (userid,urlaccess,initial_date,reason) VALUES (?,?,?,?)",values)

        ############ ENVIO DE CORREO ###################################################

        
        fromaddr = "paciente_provisional@mail.com"
        toaddr = "sist.adm.ph@gmail.com"
        msg = MIMEMultipart()
        msg['From'] = fromaddr
        msg['To'] = toaddr
        msg['Subject'] = "Solicitud de servicio de Cita"
        
        body = "El usuario: " + current_user.username + ", solicito una consulta para la categoria de: "+ access["urlaccess"] + " Para el " +access["initial_date"] 
        # button1 = """
        #         <html>
        #         <head></head>
        #         <body>
        #         <br>
        #             <td>
        #             # <a href="http://localhost:5000/login">Aprobar</a>
        #             # <a href="http://localhost:5000/login">Rechazar</a>
        #             </td>
        #         </body>
        #         </html>
        #         """
        
        msg.attach(MIMEText(body, 'plain'))
        part1 = MIMEText('html')
        # part2 = MIMEText(" \nhttp://localhost:5000/approverequest/{{access.id}}" )
        # part3 = MIMEText(" \nhttp://localhost:5000/rejectrequest/{{access.id}}")        
        msg.attach(part1)
        # msg.attach(part3)
        
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login(toaddr, "Prueba123*")
        text = msg.as_string()
        server.sendmail(fromaddr, toaddr, text)
        server.quit()
        ###############################################################################



        # user=request.form.to_dict()
        # values_user=[current_user.username,user["limited_date"]]
        # change_db("INSERT INTO user (limited_date) VALUES (?)",values_user)

        if current_user.admin_privilege == 1 or current_user.admin_privilege == 2 or current_user.admin_privilege == 3:
            return redirect(url_for("accesslist"))
        else:
            return redirect(url_for("requestdone"))

@app.route('/requestdone', methods=['GET', 'POST'])
@login_required
def requestdone():
    return render_template("requestdone.html")

@app.errorhandler(404)
def page_not_found(e):
    return render_template("404.html")

@app.route('/update/<int:id>', methods=['GET', 'POST'])
@login_required
def udpate(id):

    if request.method == "GET":
        access_list=query_db("SELECT * FROM access WHERE id=?",[id],one=True)
        return render_template("update.html",access=access_list)

    if request.method == "POST":

        print(request.form)
        access=request.form.to_dict()
        values=[access["urlaccess"],access["initial_date"],access["reason"],id]
        change_db("UPDATE access SET urlaccess=?, initial_date=?, reason=? WHERE ID=?",values)
        return redirect(url_for("accesslist"))

@app.route('/userupdate/<int:id>', methods=['GET', 'POST'])
@login_required
def userudpate(id):

    if request.method == "GET":
        user=query_db("SELECT * FROM user WHERE id=?",[id],one=True)
        return render_template("userupdate.html",user=user)

    if request.method == "POST":

        print(request.form)
        user=request.form.to_dict()
        print(user)
        values=[user["username"],user["email"],user["admin_privilege"],id]
        change_db("UPDATE user SET username=?, email=?, admin_privilege=? WHERE ID=?",values)
        return redirect(url_for("adminpanel"))


@app.route('/delete/<int:id>', methods=['GET', 'POST'])
@login_required
def delete(id):

    if request.method == "GET":
        user=query_db("SELECT * FROM user WHERE id=?",[id],one=True)
        return render_template("delete.html",user=user)

    if request.method == "POST":
        change_db("DELETE FROM user WHERE id = ?",[id])
        return redirect(url_for("adminpanel"))

@app.route('/deleterequest/<int:id>', methods=['GET', 'POST'])
@login_required
def deleterequest(id):

    if request.method == "GET":
        access=query_db("SELECT * FROM access WHERE id=?",[id],one=True)
        return render_template("deleteaccess.html")

    if request.method == "POST":
        change_db("DELETE FROM access WHERE id = ?",[id])
        return redirect(url_for("accesslist"))

@app.route('/activate/<int:id>')
def activate(id):
        change_db("UPDATE user SET Activated=1 WHERE ID=?",[id])
        return redirect(url_for("adminpanel"))

@app.route('/deactivate/<int:id>')
def deactivate(id):
        change_db("UPDATE user SET Activated=0 WHERE ID=?",[id])
        return redirect(url_for("adminpanel"))
        

@app.route('/approverequest/<int:id>')
def approverequest(id):    
        # UPDATE para Aprobacion de Solicitud
    change_db("UPDATE access SET approve=1 WHERE ID=?",[id])
   
    access_list = query_db("SELECT * FROM access WHERE ID=?", [id])
    
   
    return redirect(url_for("accesslist"))
    #return response

@app.route('/rejectrequest/<int:id>')
def rejectrequest(id):
        change_db("UPDATE access SET approve=0 WHERE ID=?",[id])
        return redirect(url_for("accesslist"))

@app.route('/addtime/<int:id>')
def addtime(id):
        now = datetime.datetime.now()
        finalDate = now + datetime.timedelta(seconds=320)
        finalDate = "'"+ finalDate.strftime('%Y-%m-%d %H:%M:%S') +"'"
        change_db("UPDATE user SET limited_date="+ finalDate +" WHERE ID=?",[id])
        return redirect(url_for("adminpanel"))

@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated == True:
        return redirect(url_for("userpanel"))
    else:
        form = LoginForm()

        if form.validate_on_submit():
            user = User.query.filter_by(username=form.username.data).first()
            if user:
                if check_password_hash(user.password, form.password.data):
                    login_user(user, remember=form.remember.data)
                    return redirect(url_for('userpanel'))
                else:
                    return render_template('login.html', form=form, error=True)
                    #return '<h1>' + form.username.data + ' ' + form.password.data + '</h1>'
        return render_template('login.html', form=form, error=False)

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    form = RegisterForm()

    if form.validate_on_submit():
        hashed_password = generate_password_hash(form.password.data, method='sha256')
        default_limited_date = datetime.datetime.now()
        print (default_limited_date)
        print (default_limited_date.strftime("%Y-%m-%d %H:%M:%S"))
        new_user = User(username=form.username.data, email=form.email.data, phone=form.phone.data, password=hashed_password, creation_date = default_limited_date.strftime("%Y-%m-%d %H:%M:%S"), admin_privilege = 0 )
        db.session.add(new_user)
        db.session.commit()

        ############ ENVIO DE CORREO ###################################################

        
        fromaddr = "paciente_provisional@mail.com"
        toaddr = "sist.adm.ph@gmail.com"
        msg = MIMEMultipart()
        msg['From'] = fromaddr
        msg['To'] = toaddr
        msg['Subject'] = "REGISTRO DE NUEVO USUARIO"
        
        body = "Se ha registrado el usuario: " + form.username.data + ", favor contactar al correo: "+ form.email.data + " o al telefono:  " + form.phone.data
      
        
        msg.attach(MIMEText(body, 'plain'))             
        
        server = smtplib.SMTP('smtp.gmail.com', 587)
        server.starttls()
        server.login(toaddr, "Prueba123*")
        text = msg.as_string()
        server.sendmail(fromaddr, toaddr, text)
        server.quit()



        return render_template("base.html")
        # return '<h1>Ha sido creado como nuevo usuario!! ' + form.username.data + ' ' + form.email.data + ' ' + default_limited_date.strftime("%Y-%m-%d %H:%M:%S") + ' </h1> '

    return render_template('signup.html', form=form)

@app.route('/userpanel')
@login_required
def userpanel():
    if current_user.admin_privilege == 0: 
        return render_template('userpanel.html', user=current_user)
    else:
        return redirect(url_for('adminpanel'))

@app.route('/switch_1')
@login_required
def switch_1():
    access_to_switch = False
    if current_user.admin_privilege == 0: 
    # '0' meaning a normal user. '1' would be an admin user. '2' is an admin user with total admin privilege that can't be erased or modified.
    # a '2' level admin privilege user would sometimes be refered as 'the system'. 
        user_access_list = query_db("SELECT * FROM access WHERE userid=?",[current_user.username])
        for accessrequest in user_access_list:

            if "SWITCH" in accessrequest["urlaccess"] and accessrequest["approve"] == 1:
               access_to_switch = True
               access_limited_date = accessrequest["limited_date"]
               access_initial_date = accessrequest["initial_date"]
            #    break
               
            elif "STREAMING" in accessrequest["urlaccess"] and accessrequest["approve"] == 1:
                access_to_switch = True
                access_limited_date = accessrequest["limited_date"]
                access_initial_date = accessrequest["initial_date"]
                # break
            elif "SOCIAL_NETWORK" in accessrequest["urlaccess"] and accessrequest["approve"] == 1:
                access_to_switch = True
                access_limited_date = accessrequest["limited_date"]
                access_initial_date = accessrequest["initial_date"]
                # break
            elif "BANKING" in accessrequest["urlaccess"] and accessrequest["approve"] == 1:
               access_to_switch = True
               access_limited_date = accessrequest["limited_date"]
               access_initial_date = accessrequest["initial_date"]
            #    break
            elif "WEB_CHAT" in accessrequest["urlaccess"] and accessrequest["approve"] == 1:
               access_to_switch = True
               access_limited_date = accessrequest["limited_date"]
               access_initial_date = accessrequest["initial_date"]
            #    break

    else:
        return redirect(url_for('adminpanel'))

    if access_to_switch == True:
        initial_date = datetime.datetime.strptime(access_initial_date, '%Y-%m-%d %H:%M:%S')
        leaving_date = datetime.datetime.strptime(access_limited_date, '%Y-%m-%d %H:%M:%S')
        now = datetime.datetime.now()
        #dti = daysHoursMinutesSecondsFromSeconds(dateDiffInSeconds(now, initial_date))
        #dtf = daysHoursMinutesSecondsFromSeconds(dateDiffInSeconds(now, leaving_date))

        if now > initial_date and now < leaving_date:
            return render_template('switch_1.html', user=current_user)
        else:
            return render_template('no_access.html', error=2, user=current_user)
    else:
        return render_template('no_access.html', error=1, user=current_user)

@app.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))

if __name__ == '__main__':
    app.run(debug=True)
